export default class Page {
  startPage() {
    const { pageName } = this;
    document.addEventListener("DOMContentLoaded", () => {
      console.log(`${pageName} init`);
      this.init();
      this.events();
    });

    window.addEventListener("load", () => {
      console.log(`${pageName} onLoad`);
      this.onLoad();
    });
  }

  init() {}
  events() {}
  onLoad() {}
}

import Page from "../Page";
import axios from "axios";
import validateEmail from "../../tools/validateEmail";
import { serializeObj } from "../../tools/serializes";
import debounce from "../../tools/debounce";

export default class Common extends Page {
  clearForm(form) {
    const elements = form.elements;

    for (let i = 0; i < elements.length; i++) {
      elements[i].value = "";
    }
  }

  validateForm(serialized) {
    for (const key in serialized) {
      if (!serialized[key].length) return false;

      if (key === "email") {
        if (!validateEmail(serialized[key])) return false;
      }
    }

    return true;
  }

  newsletterHandleSubmit(evt) {
    evt.preventDefault();
    const { clearForm } = this;
    const form = evt.currentTarget;
    const serialized = serializeObj(form);

    if (!serialized) return false;

    if (!this.validateForm(serialized)) return false;

    axios.patch("/api/newsletter", serialized).then(res => {
      console.log("res>>>", res);
      clearForm(form);
      alert("Cadastro realizado com sucesso.");
    });
  }

  handleScroll(evt) {
    const header = document.getElementsByClassName("mainHeader")[0],
      { pageYOffset } = window;
    const { offsetHeight } = header;

    if (pageYOffset > offsetHeight * 1.5) {
      header.classList.add("tiny");
    } else {
      header.classList.remove("tiny");
    }
  }

  events() {
    const { newsletterHandleSubmit, handleScroll } = this;
    const newsFooter = document.getElementsByClassName("mainFooter__form");
    newsFooter.length &&
      newsFooter[0].addEventListener(
        "submit",
        newsletterHandleSubmit.bind(this)
      );

    window.addEventListener("scroll", debounce(handleScroll.bind(this), 40));
  }
}

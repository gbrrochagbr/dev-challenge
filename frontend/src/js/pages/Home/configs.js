export const bannerConfig = {
  container: ".mainBanner",
  items: 1,
  slideBy: "page",
  autoplay: true,
  nav: false,
  autoplayHoverPause: true
};

import Page from "../Page";
import axios from "axios";
import { tns } from "tiny-slider/src/tiny-slider";
import { bannerConfig } from "./configs";

export default class Home extends Page {
  startSlider() {
    this.slider = tns(bannerConfig);
  }

  init() {
    axios.get("/api/products").then(res => {
      console.log("res>>", res);
    });

    this.startSlider();
  }
}

import Common from "./pages/Common";
import Home from "./pages/Home";

class Main {
  constructor() {
    this.pages = {
      common: Common,
      home: Home
    };
    this.instantiatePages();
  }

  instantiatePages() {
    let { pages } = this;
    const body = document.getElementsByTagName("body")[0];

    //Starts page's scripts
    for (const page in pages) {
      if (body.classList.contains(page) || page == "common") {
        pages[page] = new pages[page]();
        pages[page].pageName = page;
        pages[page].startPage();
      }
    }
  }
}

new Main();

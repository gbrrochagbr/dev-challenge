import Express from "express";
import fs from "fs";
import { fileURLToPath } from "url";
import path, { dirname } from "path";
import bodyParser from "body-parser";

function getProducts(cb) {
  fs.readFile(path.join(__dirname, "/data/products.json"), function(err, data) {
    cb && cb(JSON.parse(data));
  });
}

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

const server = Express();
server.use(bodyParser.json());
server.use(Express.static(path.join(__dirname, "/public")));

//Routes
server.get("/api/products", (req, res) => {
  return getProducts(function(data) {
    res.json(data);
  });
});

server.patch("/api/newsletter", (req, res) => {
  console.log(req.body);
  setTimeout(() => {
    res.status(200).send(req.body);
  }, 400); //Fake persist
});
//Routes

console.log("listening port 3333");
server.listen(3333);

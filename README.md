# Desafio FrontEnd

## Instruções para rodar o projeto

### Dependências Globais:

- node 13.6;
- gulp-cli 2.2.0;
- yarn 1.16.0;

### Execução:

- Basta executar 'yarn start'.(Caso esteja no mac ou linux, recomendável usar sudo);
- O script start instalará as dependências locais, fará o build dos arquivos e subirá um servidor local.
- O navegador deverá abrir automaticamente no endereço raiz do site. Caso não abra, basta abrir a url http://localhost:5000 para visualizar a página.

#### Contato:

###### Nome: Gabriel Rocha

###### Email: gbrrochagbr@gmail.com

# ---------------------------------------------------------------------------

## Instruções para o teste

- Dê um fork neste projeto;
- Desenvolva as telas;
- Atualize o readme com as instruções necessárias para rodar o seu código;
- Faça um pull request.

##### Sugestões de implementação

- Interação com JSON para renderizar os produtos (você vai encontrar um mockup em src/data/products.json)
- Filtro de produtos funcional
- Adicionar produtos ao carrinho
- Botão de carregar mais produtos

##### Dicas

- Evite usar linguagens, ferramentas e metodologias que não domine;
- Não esqueça de manter o package atualizado com os módulos necessários para rodar seu projeto;

###### Dúvidas: vinicius.diniz@somagrupo.com.br
